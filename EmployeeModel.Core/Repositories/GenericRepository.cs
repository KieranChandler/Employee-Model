﻿using System;
using System.Collections.Generic;
using System.Text;
using EmployeeModel.Core.Models;

namespace EmployeeModel.Core.Repositories
{
    interface GenericRepository<T>
    {
        IEnumerable<T> GetAll();

        IEnumerable<T> GetById(Guid EntityId);
    }
}
