﻿using System;
using System.Collections.Generic;
using System.Text;
using EmployeeModel.Core.Models;

namespace EmployeeModel.Core.Repositories
{
    interface EmployeeRepository
    {
        IEnumerable<Employee> GetAllEmployees();
        IEnumerable<Employee> GetEmployeeById(Guid EmployeeId);

    }
}
