﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace EmployeeModel.Core.Models
{
    public class EntityBase
    {
        [Key]
        public Guid Id { get; set; }
    }
}
