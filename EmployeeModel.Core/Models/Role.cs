﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace EmployeeModel.Core.Models
{
    public class Role : EntityBase
    {
        [Required]
        [MaxLength(64)]
        public string Description {get; set;}
    }
}
