﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace EmployeeModel.Core.Models
{
    public class Employee : EntityBase
    {
        [Required]
        [MaxLength(64)]
        public string Name { get; set; }

        public DateTime DOB { get; set; }

        public IEnumerable<Role> Roles { get; set; }
    }
}
